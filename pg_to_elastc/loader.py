from db.es_db import ElasticBase
from elasticsearch import helpers
from typing import Generator
from loguru import logger


class ESLoader(ElasticBase):

    def generate_elastic_data(self, index_name, data: list) -> Generator:
        for item in data:
            movie = {
                '_id': item.id,
                '_index': index_name,
                **item.dict(),
            }
            yield movie

    def save_bulk(self, index, data: list) -> None:
        res, _ = helpers.bulk(
            self.client,
            self.generate_elastic_data(index, data)
        )
        logger.info(f'Синхронизированно записей {res}')
